module.exports = function override(config, env) {
	const hostUrl = `http://${env.HOST || 'localhost:8085'}`
	const oldConfig = config.devServer
	config.devServer = {
		...oldConfig,
		proxy: {
			'/auth/*': hostUrl,
			'/api/*': hostUrl,
			'/fin5Lang/*': hostUrl,
			'/finStackAuth': hostUrl,
			'/finWebApp/finstack': hostUrl,
			'/finGetFile/*': hostUrl,
			'/user/*': hostUrl,
			'/fin5/*': hostUrl,
			'/finPod/*': hostUrl,
			'/pod/*': hostUrl,
		},
	}

	return config
}