import React, { useEffect, useState } from 'react';
import { Client } from 'haystack-nclient';
import { ClientContext } from 'haystack-react';
import { HDict, HGrid } from 'haystack-core';
import { Input, Table } from 'antd';

import 'antd/es/table/style/css'
import './App.css'


const client = new Client({base: new URL(window.location.href), project: 'demo'})

function App() {
  const [loading, setLoading] = useState(false)
  const [filter, setFilter] = useState('site')
  const [result, setResult] = useState<HGrid>()

  useEffect(() => {

    (async () => {
      setLoading(true)
      const grid = await client.record.readByFilter(filter)
      setResult(grid)
      setLoading(false)
    })()

  }, [filter])
  return (
    <ClientContext.Provider value={client}>
      <header style={{padding: 10, borderBottom: '1px solid #d0d0d0'}}>
        <Input.Search placeholder='enter filter...' enterButton='Go' onSearch={(val) => setFilter(val)}/>
      </header>
      <div className='lastQuery' style={{backgroundColor: 'rgba(24, 144, 255, 0.05)', padding: '3px 10px', borderBottom: '1px solid #d0d0d0'}}>
      &raquo; {filter}
      </div>
      <div className='container'>
      <Table dataSource={result?.getRows()} columns={result?.getColumns().map(c => ({
      title: c.dis,
      dataIndex: c.name,
      key: c.name,
      render(value: unknown, record: HDict, index: number) {
        return record.has(c.name) ? String(record.get(c.name)) : ''
      },
    }))} className='gridTable' pagination={{pageSize:50}} loading={loading}/>
      </div>
    </ClientContext.Provider>
  );
}

export default App;
