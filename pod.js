const fs = require('fs')
const path = require('path')
const archiver = require('archiver')

const pkg = require('./package.json')

const PATH_DESTINATION = path.resolve(__dirname, pkg.pod.destination || './dist')
const BUILD_PATH = path.resolve(__dirname, pkg.pod.buildPath || './build')

const objToProps = (obj) => {
	let props = ''

	for (const key of Object.keys(obj)) {
		const val = obj[key]
		if (typeof val === 'object') {
			props += objToProps(val)
		} else {
			props += `${key}=${val}\n`
		}
	}

	return props
}

const podName = () => {
	return `${pkg.name.replace(/-[a-zA-Z]/g, (m) => m.replace('-', '').toUpperCase())}Ext`
}

const makeIndexProps = (p) => {
	return {
		skyarc: {
			ext: podName()
		}
	}
}

const makeMetaProps = (p) => {
	return {
		pod: {
			name: podName(),
			version: pkg.version,
			isScript: false,
			depends: pkg.pod.depends || 'sys 1.0',
			build: {
				ts: new Date().toISOString()
			},
			org: {
				name: pkg.pod.org || '',
				uri: pkg.pod.uri || '',
			},
			license: {
				name: 'Commercial'
			},
			ext: {
				name: podName().replace(/Ext$/, '')
			}
		}
	}
}

const clean = () => {
	if (fs.existsSync(PATH_DESTINATION)) {
		fs.rmSync(PATH_DESTINATION, {recursive: true})
	}

	fs.mkdirSync(PATH_DESTINATION)
}

//	start here...
const init = () => {
	clean()
	const fileName = `${PATH_DESTINATION}/${podName()}.pod`
	console.log('Writing to -> ', fileName)
	const output = fs.createWriteStream(path.resolve(fileName))
	const zip = archiver('zip')

	output.on('close', () => {
		console.log(zip.pointer() + ' total bytes')
		console.log('Done.')
	})

	zip.pipe(output)

	//	add index props
	zip.append(objToProps(makeIndexProps(pkg)), {name: 'index.props'})

	//	add meta props
	zip.append(objToProps(makeMetaProps(pkg)), {name: 'meta.props'})

	//	add the build dir contents
	zip.directory(`${BUILD_PATH}/`, false)

	//	seal zip
	zip.finalize()
}

init()


